package com.example.simplechatclient;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MessagesAdapter extends ArrayAdapter<Message> {
    public MessagesAdapter(Context context, ArrayList<Message> messages) {
        super(context, 0, messages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Message message = getItem(position);

        boolean isMe;
        isMe = message.from.equals(message.me);

//        Log.e("Adapter", message.from + " " + message.me + ": " + String.valueOf(isMe));
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            if (isMe) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.from_message, parent, false);
            } else
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.to_message, parent, false);

        }

        TextView msg, date;

        if (isMe) {
            // Lookup view for data population
            msg = convertView.findViewById(R.id.TEFromM);
            date = convertView.findViewById(R.id.TEFromD);
        } else {
            msg = convertView.findViewById(R.id.TEToM);
            date = convertView.findViewById(R.id.TEToD);
        }
        DateFormat df = new SimpleDateFormat("dd.mm hh:mm:ss");
        String dt_str = df.format(message.createdOn);

        // Populate the data into the template view using the data object
        msg.setText(message.body);
        date.setText(dt_str);
        // Return the completed view to render on screen
        return convertView;
    }
}