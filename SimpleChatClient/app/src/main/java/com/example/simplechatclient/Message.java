package com.example.simplechatclient;

import java.util.Date;

public class Message {
    public String body;
    public Date createdOn;
    public String from;
    public String me;

    public Message(String body, long utime, String from, String me){
        this.body = body;
        this.createdOn = new Date((long) utime);
        this.from = from;
        this.me = me;
    }
}
