package com.example.simplechatclient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesActivity extends AppCompatActivity {

    String token, userMe, userTo;
    String room_id;
    JSONObject room;
    JSONArray messages;
    ArrayList<Message> msg_list;
    ArrayAdapter<Message> arrayAdapter;
    long last_fetched_at = 0;

    ServerConsts url = new ServerConsts();

    private Toolbar mTopToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_messages);

            Intent intent = getIntent();

            String raw_json;
            raw_json = intent.getStringExtra("json");
            token = intent.getStringExtra("token");
            userMe = intent.getStringExtra("userMe");
            userTo = intent.getStringExtra("userTo");
//
//            mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//            mTopToolbar.setTitle(userTo);
//            setSupportActionBar(mTopToolbar);

            Log.w("Brr", userMe);

            room = new JSONObject(raw_json);
            messages = room.getJSONArray("messages");
            room_id =  room.get("room_id").toString();

            ListView LVRoom = (ListView)findViewById(R.id.LVRoom);
            msg_list = new ArrayList<Message>();
//            LinearLayout LOChat = new LinearLayout(this, null, android.R.style.DeviceDefault_Light_ButtonBar);
            for (int i = 0; i < messages.length(); i++) {
                JSONObject message = messages.getJSONObject(i);
                String msg_text = message.get("message").toString();
                String from = message.get("from").toString();
                long date = (long) message.getDouble("sent_on") * 1000;
                if (date > last_fetched_at) {
                    last_fetched_at = date;
                }

                Message msg = new Message(msg_text, date, from, userMe);
                msg_list.add(msg);

            }
            arrayAdapter = new MessagesAdapter(this, msg_list);
            LVRoom.setAdapter(arrayAdapter);
        } catch (Exception e) {
            Log.e("Chats.onCreate", e.toString());
        }
    }
    public void Send(View view) {
        /* Do something in response to button click */
        String body;
        try {

            TextView TEMessage = findViewById(R.id.TEMessage);

            body = TEMessage.getText().toString();
            if (body.length() == 0)
                return;

            TEMessage.setText("");
            Map<String, String> postData = new HashMap<>();
            postData.put("message", body);
            postData.put("token", token);
            postData.put("room_id", room_id);
            MessagesActivity.SendPost task = new MessagesActivity.SendPost(postData);
            task.execute(url.send);

        } catch (Exception e){
            Log.w("Send", e.getLocalizedMessage());
        }
    }

    public void Fetch(View view) {
        Map<String, String> postData = new HashMap<>();
        postData.put("since", String.valueOf(last_fetched_at));
        postData.put("token", token);
        postData.put("room_id", room_id);
        MessagesActivity.SendPost task = new MessagesActivity.SendPost(postData);
        task.execute(url.fetch);
    }


    class PostResponse {
        public int return_code = 400;
        public String json;

        public PostResponse(int return_code, String json) {
            this.return_code = return_code;
            this.json = json;
        }

        public int getCode() {
            return return_code;
        }

        public String getJson() {
            return json;
        }
    }

    class SendPost extends AsyncTask<String, Void, MessagesActivity.PostResponse>
    {
        JSONObject postData;
        MessagesActivity.PostResponse post_response;

        public SendPost(Map<String, String> postData) {
            if (postData != null) {
                this.postData = new JSONObject(postData);
            }
        }

        @Override
        protected MessagesActivity.PostResponse doInBackground(String... params){
            try {
                // This is getting the url from the string we passed in
                URL url = new URL(params[0]);

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                urlConnection.setRequestProperty("Content-Type", "application/json");

                urlConnection.setRequestMethod("POST");


                // OPTIONAL - Sets an authorization header
                urlConnection.setRequestProperty("Authorization", "someAuthString");

                // Send the post body
                if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }

                int statusCode = urlConnection.getResponseCode();

                InputStream inputStream;
                if (statusCode >= 200 && statusCode < 400) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
                else {
                    inputStream = new BufferedInputStream(urlConnection.getErrorStream());
                }
                byte[] contents = new byte[1024];

                int bytesRead = 0;
                String response = "";
                while((bytesRead = inputStream.read(contents)) != -1) {
                    response += new String(contents, 0, bytesRead);
                }

                post_response = new MessagesActivity.PostResponse(statusCode, response);

            } catch (Exception e) {
                Log.e("SendPost", e.toString());
                throw new RuntimeException(e);
            }
            return post_response;
        }

        @Override
        protected void onPostExecute(MessagesActivity.PostResponse response) {
            try {
                super.onPostExecute(response);
                String message;
                JSONObject resp_json;
                JSONArray messages;

                if (response.return_code == 200) {
//                    Refresh(response.json);
                    resp_json = new JSONObject(response.json);
                    messages = resp_json.getJSONArray("messages");

                    for (int i = 0; i < messages.length(); i++) {
                        JSONObject msg = messages.getJSONObject(i);
                        String msg_text = msg.get("message").toString();
                        String from = msg.get("from").toString();
                        long date = (long) msg.getDouble("sent_on") * 1000;


                        Message new_msg = new Message(msg_text, date, from, userMe);
                        msg_list.add(new_msg);

                        arrayAdapter.notifyDataSetChanged();
                    }
                    last_fetched_at = System.currentTimeMillis();
                }
                else if (response.return_code == 201) {
                    Map<String, String> postData = new HashMap<>();
                    postData.put("since", String.valueOf(last_fetched_at));
                    postData.put("token", token);
                    postData.put("room_id", room_id);
                    MessagesActivity.SendPost task = new MessagesActivity.SendPost(postData);
                    task.execute(url.fetch);
                } else {
                    resp_json = new JSONObject(response.json);
                    message = resp_json.get("error_msg").toString();
                    Toast.makeText(MessagesActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e("Fuck", e.toString());
            }
        }
    }


}
