package com.example.simplechatclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ChatsActivity extends AppCompatActivity {

    String token, userMe;
    JSONObject chats;
    JSONArray users;

    ServerConsts url = new ServerConsts();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_chats);

            Intent intent = getIntent();

            String raw_json;
            raw_json = intent.getStringExtra("json");
            userMe = intent.getStringExtra("userMe");

            chats = new JSONObject(raw_json);
            users = chats.getJSONArray("users");
            token = chats.get("token").toString();

            LinearLayout LOChat = findViewById(R.id.LOChats);
//            LinearLayout LOChat = new LinearLayout(this, null, android.R.style.DeviceDefault_Light_ButtonBar);
            for (int i = 0; i < users.length(); i++) {
                JSONObject user = users.getJSONObject(i);
                String username = user.get("username").toString();

                Button chat = new Button(this);
//                chat.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                chat.setText(username);
                chat.setId(i);
                chat.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Button b = (Button)v;
                        Toast.makeText(ChatsActivity.this, b.getText(), Toast.LENGTH_LONG).show();

                        try {

                            Map<String, String> postData = new HashMap<>();
                            postData.put("token", token);
                            postData.put("user_to", b.getText().toString());
                            ChatsActivity.SendPost task = new ChatsActivity.SendPost(postData);
                            task.execute(url.room);

                        } catch (Exception e){
                            Log.w("LogIn", e.getLocalizedMessage());
                        }
                    }
                });

                LOChat.addView(chat);

            }
        } catch (Exception e) {
            Log.e("Chats.onCreate", e.toString());
        }
    }

    public void GoToMessagesView(String json, String userTo) {
        Intent intent = new Intent(this, MessagesActivity.class);
        intent.putExtra("json", json);
        intent.putExtra("token", token);
        intent.putExtra("userTo", userTo);
        intent.putExtra("userMe", userMe);
        startActivity(intent);
    }


    class PostResponse {
        public int return_code = 400;
        public String json;

        public PostResponse(int return_code, String json) {
            this.return_code = return_code;
            this.json = json;
        }

        public int getCode() {
            return return_code;
        }

        public String getJson() {
            return json;
        }
    }

    class SendPost extends AsyncTask<String, Void, ChatsActivity.PostResponse>
    {
        JSONObject postData;
        ChatsActivity.PostResponse post_response;

        public SendPost(Map<String, String> postData) {
            if (postData != null) {
                this.postData = new JSONObject(postData);
            }
        }

        @Override
        protected ChatsActivity.PostResponse doInBackground(String... params){
            try {
                // This is getting the url from the string we passed in
                URL url = new URL(params[0]);

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                urlConnection.setRequestProperty("Content-Type", "application/json");

                urlConnection.setRequestMethod("POST");


                // OPTIONAL - Sets an authorization header
                urlConnection.setRequestProperty("Authorization", "someAuthString");

                // Send the post body
                if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }

                int statusCode = urlConnection.getResponseCode();

                InputStream inputStream;
                if (statusCode >= 200 && statusCode < 400) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
                else {
                    inputStream = new BufferedInputStream(urlConnection.getErrorStream());
                }
                byte[] contents = new byte[1024];

                int bytesRead = 0;
                String response = "";
                while((bytesRead = inputStream.read(contents)) != -1) {
                    response += new String(contents, 0, bytesRead);
                }

                post_response = new ChatsActivity.PostResponse(statusCode, response);

            } catch (Exception e) {
                Log.e("SendPost", e.toString());
                throw new RuntimeException(e);
            }
            return post_response;
        }

        @Override
        protected void onPostExecute(ChatsActivity.PostResponse response) {
            try {
                super.onPostExecute(response);
                String message;
                JSONObject resp_json;

                if (response.return_code == 200)
                    GoToMessagesView(response.json, postData.getString("user_to"));
                else if (response.return_code == 201) {
                    message = "User created successfully.";
                    Toast.makeText(ChatsActivity.this, message, Toast.LENGTH_LONG).show();
                } else {
                    resp_json = new JSONObject(response.json);
                    message = resp_json.get("error_msg").toString();
                    Toast.makeText(ChatsActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e("Fuck", e.toString());
            }
        }
    }

////    @Override
//    public void onClick(View v) {
//        Toast.makeText(getApplicationContext(), v.getResources().getResourceEntryName(v.getId()), Toast.LENGTH_LONG).show();
////        switch (v.getId()) {
////            case R.id.button2:
////                Toast.makeText(getApplicationContext(), "Programmatic Button is clicked", Toast.LENGTH_LONG).show();
////                break;
////            case R.id.button3:
////                Toast.makeText(getApplicationContext(), "Another Programmatic Button is clicked", Toast.LENGTH_LONG).show();
////                break;
////
////        }
//    }
}
