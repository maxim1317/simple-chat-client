package com.example.simplechatclient;

public class ServerConsts {
//    String host = "10.0.1.75";
//    String host = "207.154.216.232";
//    String port = "1313";
    String host = "simple-chat-nastasjushka.herokuapp.com";
    String port = ":";

    String base = "http://" + host + port;
    String login = base + "/login";
    String register = base + "/register";
    String room = base + "/room";
    String send = room + "/send";
    String fetch = room + "/fetch";
}
