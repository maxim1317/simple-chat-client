package com.example.simplechatclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpRetryException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    Button BLogin, BRegister;
    EditText TELogin, TEPass;
    ServerConsts url = new ServerConsts();

    class PostResponse {
        public int return_code = 400;
        public String json;

        public PostResponse(int return_code, String json) {
            this.return_code = return_code;
            this.json = json;
        }

        public int getCode() {
            return return_code;
        }

        public String getJson() {
            return json;
        }
    }
    public void GoToChatView(String json, String userMe) {
        Intent intent = new Intent(this, ChatsActivity.class);
        intent.putExtra("json", json);
        intent.putExtra("userMe", userMe);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TELogin = findViewById(R.id.TELogin);
        TEPass = findViewById(R.id.TEPass);
        BLogin = findViewById(R.id.BLogin);
        BRegister = findViewById(R.id.BRegister);

//        BLogin.setOnClickListener(
//                new View.OnClickListener()
//                {
//                    public void OnClick(View view)
//                    {
//                        Log.w("Login", TELogin.getText().toString());
//                    }
//                }
//        );
    }

    public void LogIn(View view) {
        /* Do something in response to button click */
        try {

            Map<String, String> postData = new HashMap<>();
            postData.put("username", TELogin.getText().toString());
            postData.put("password", TEPass.getText().toString());
            SendPost task = new SendPost(postData);
            task.execute(url.login);

        } catch (Exception e){
            Log.w("LogIn", e.getLocalizedMessage());
        }
    }

    public void SignUp(View view) {
        /* Do something in response to button click */
        try {

            Map<String, String> postData = new HashMap<>();
            postData.put("username", TELogin.getText().toString());
            postData.put("password", TEPass.getText().toString());
            SendPost task = new SendPost(postData);


            task.execute(url.register);

        } catch (Exception e){
            Log.w("LogIn", e.getLocalizedMessage());
        }
    }

    class SendPost extends AsyncTask<String, Void, PostResponse>
    {
        JSONObject postData;
        PostResponse post_response;

        public SendPost(Map<String, String> postData) {
            if (postData != null) {
                this.postData = new JSONObject(postData);
            }
        }

        @Override
        protected PostResponse doInBackground(String... params){
            try {
                // This is getting the url from the string we passed in
                URL url = new URL(params[0]);

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                urlConnection.setRequestProperty("Content-Type", "application/json");

                urlConnection.setRequestMethod("POST");


                // OPTIONAL - Sets an authorization header
                urlConnection.setRequestProperty("Authorization", "someAuthString");

                // Send the post body
                if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }

                int statusCode = urlConnection.getResponseCode();

                InputStream inputStream;
                if (statusCode >= 200 && statusCode < 400) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
                else {
                    inputStream = new BufferedInputStream(urlConnection.getErrorStream());
                }
                byte[] contents = new byte[1024];

                int bytesRead = 0;
                String response = "";
                while((bytesRead = inputStream.read(contents)) != -1) {
                    response += new String(contents, 0, bytesRead);
                }

                post_response = new PostResponse(statusCode, response);

            } catch (Exception e) {
                Log.e("SendPost", e.toString());
                throw new RuntimeException(e);
            }
            return post_response;
        }

        @Override
        protected void onPostExecute(PostResponse response) {
            try {
                super.onPostExecute(response);
                String message;
                JSONObject resp_json;

                if (response.return_code == 200)
                    GoToChatView(response.json, postData.getString("username"));
                else if (response.return_code == 201) {
                    message = "User created successfully.";
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                } else {
                    resp_json = new JSONObject(response.json);
                    message = resp_json.get("error_msg").toString();
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e("Fuck", e.toString());
            }
        }
    }

}
